# JP Word Wrap

日本語の禁則処理を行うライブラリです。

## Features

* 記号の行頭（ぶら下り）/行末（追い出し）
* 数字や英単語の分離禁止（追い出し）

## Objects

### JPWordWrapLabel

Labelに禁則処理機能を入れたもの。original_textに文字列を入れると禁則処理を行い、textを更新して表示する。

## Functions

### JPWordWrap.word_wrap_split_by_length( src:String, length_per_line:int ) -> PoolStringArray

一行の文字数を指定して、禁則処理を行いつつ文を区切る。

* src: 文字列
* length_per_line: 一行あたりの文字数

### JPWordWrap.word_wrap_split_by_width( src:String, font:Font, width:float ) -> PoolStringArray

フォントの文字サイズで指定した幅に一行が収まるように禁則処理を行いつつ文を区切る。プロポーショナルフォントでも動く。

* src: 文字列
* font: 表示に使用するフォント
* width: 一行の幅（pixel単位）

## License

フォント以外はMIT License。

## Author

あるる / きのもと 結衣 @arlez80
