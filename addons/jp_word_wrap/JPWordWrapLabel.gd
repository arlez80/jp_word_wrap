extends Label

const JPWordWrap: = preload("jp_word_wrap.gd")

export(String) var original_text:String = "" setget set_original_text

func _ready( ):
	pass

func set_original_text( s:String ):
	if original_text != s:
		self.text = JPWordWrap.word_wrap_split_by_width( s, self.get_font( "font" ), self.rect_size.x ).join( "\n" )
		original_text = s

#func _on_JPWordWrapLabel_resized( ):
#	self.text = JPWordWrap.word_wrap_split_by_width( self.original_text, self.get_font( "font" ), self.rect_size.x ).join( "\n" )
