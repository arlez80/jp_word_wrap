"""
	Japanese Word Wrap functions by Yui Kinomoto @arlez80
"""

tool
extends EditorPlugin

func _enter_tree():
	self.add_custom_type( "JPWordWrapLabel", "Label", preload("JPWordWrapLabel.gd"), null )

func _exit_tree():
	self.remove_custom_type( "JPWordWrapLabel" )

func has_main_screen( ):
	return true

func make_visible( visible:bool ):
	pass

func get_plugin_name( ):
	return "JP Word Wrap Label"
