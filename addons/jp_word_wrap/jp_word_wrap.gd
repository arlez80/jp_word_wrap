"""
	Japanese Word Wrap functions by Yui Kinomoto @arlez80
"""

# 行頭禁則文字
const banned_first_characters:String = ",)]）］｝、〕〉》」』】〙〗〟'\"｠»ゝゞーァィゥェォッャュョヮヵヶぁぃぅぇぉっゃゅょゎゕゖㇰㇱㇲㇳㇴㇵㇶㇷㇸㇹㇷㇺㇻㇼㇽㇾㇿ々〻‐゠–〜～・:;/!?！？。."
# 行末禁則文字
const banned_last_characters:String = "([（［｛〔〈《「『【〘〖〝'\"｟«"
# 分離禁止グループ
const banned_split_characters_table:PoolStringArray = PoolStringArray([
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ",
	"0123456789０１２３４５６７８９〇零一二三四五六七八九十百千万億兆",
])

"""
	指定したBBコードを含まない文字数の文字列を取得
"""
static func _get_string_length_without_bb_code( buffer:String, line_length:int ) -> int:
	var remain:int = line_length
	var p:int = 0
	var bb_code:bool = false
	while p < len( buffer ) and 0 < remain:
		if buffer[p] == "]":
			bb_code = false
		if buffer[p] == "[":
			bb_code = true
		if not bb_code:
			remain -= 1
		p += 1

	return p

"""
	一行の文字数以内に収まるように禁則処理をする
	@param	src				文字列
	@param	length_per_line	一行の文字数
	@param	save_bb_codes	BBコードを考慮するようにする
	@return	禁則処理を施した文字列配列
"""
static func word_wrap_split_by_length( src:String, length_per_line:int, save_bb_codes:bool = false ) -> PoolStringArray:
	var splited_text:Array = []
	var buffer:String = src

	while 0 < len( buffer ):
		var line_length:int = len( buffer ) if len( buffer ) < length_per_line else length_per_line

		if save_bb_codes:
			line_length = _get_string_length_without_bb_code( buffer, line_length )
		var line:String = buffer.substr( 0, line_length )
		buffer = buffer.substr( line_length )

		# 行頭禁則文字（ぶら下げ）
		if 0 < len( buffer ) and banned_first_characters.find( buffer[0] ) != -1:
			line += buffer[0]
			buffer = buffer.substr( 1 )
		# 行末禁則文字（追い出し）
		if 0 < len( buffer ) and  banned_last_characters.find( line[-1] ) != -1:
			buffer = line[-1] + buffer
			line = line.substr( 0, len( line ) - 1 )
		# 分離禁止（追い出しのみ）
		for table in banned_split_characters_table:
			if 0 < len( buffer ) and table.find( buffer[0] ) != -1:
				var temp_buffer:String = buffer
				var temp_line:String = line
				while 0 < len( temp_line ) and table.find( temp_line[-1] ) != -1:
					temp_buffer = temp_line[-1] + temp_buffer
					temp_line = temp_line.substr( 0, len( temp_line ) - 1 )
				if 0 < len( temp_line ):
					buffer = temp_buffer
					line = temp_line
		splited_text.append( line )

	return PoolStringArray( splited_text )

"""
	フォントと幅から一行に収まるように禁則処理をする

	@param	src		文字列
	@param	font	表示に使用するフォント
	@param	width	一行の最大幅
	@param	save_bb_codes	BBコードを考慮するようにする
"""
static func word_wrap_split_by_width( src:String, font:Font, width:float, save_bb_codes:bool = false ) -> PoolStringArray:
	var splited_text:Array = []
	var buffer:String = src

	var bb_codes:Array = []
	if save_bb_codes:
		var bb_code_cutter:RegEx = RegEx.new( )
		bb_code_cutter.compile( "\\[.+?\\]" )
		for t in bb_code_cutter.search_all( buffer ):
			bb_codes.append([
				t.get_start( )
			,	t.get_string( )
			])
		buffer = bb_code_cutter.sub( buffer, "", true )

	while 0 < len( buffer ):
		# 1行に出せそうな文字数を探索
		var p_min:int = 0
		var p_max:int = len( buffer )
		while p_min < p_max:
			var p_mid:int = p_min + ( p_max - p_min ) / 2
			var temp_line:String = buffer.substr( 0, p_mid )
			var line_width:float = font.get_string_size( temp_line ).x
			if width < line_width:
				p_max = p_mid - 1
			elif line_width < width:
				p_min = p_mid + 1
			else:
				p_max = p_mid
				break

		var line_length:int = p_max
		var line:String = buffer.substr( 0, line_length )
		buffer = buffer.substr( line_length )

		# 行頭禁則文字（ぶら下げ）
		if 0 < len( buffer ) and banned_first_characters.find( buffer[0] ) != -1:
			line += buffer[0]
			buffer = buffer.substr( 1 )
		# 行末禁則文字（追い出し）
		if 0 < len( buffer ) and  banned_last_characters.find( line[-1] ) != -1:
			buffer = line[-1] + buffer
			line = line.substr( 0, len( line ) - 1 )
		# 分離禁止（追い出しのみ）
		for table in banned_split_characters_table:
			if 0 < len( buffer ) and table.find( buffer[0] ) != -1:
				var temp_buffer:String = buffer
				var temp_line:String = line
				while 0 < len( temp_line ) and table.find( temp_line[-1] ) != -1:
					temp_buffer = temp_line[-1] + temp_buffer
					temp_line = temp_line.substr( 0, len( temp_line ) - 1 )
				if 0 < len( temp_line ):
					buffer = temp_buffer
					line = temp_line
		splited_text.append( line )

	if save_bb_codes:
		for t in bb_codes:
			var p:int = 0
			for i in range( splited_text.size( ) ):
				var size:int = len( splited_text[i] )
				if t[0] < p + size:
					splited_text[i] = splited_text[i].insert( t[0] - p, t[1] )
					break
				p += size

	return PoolStringArray( splited_text )
